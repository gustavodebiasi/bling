<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesafioCincoController extends Controller
{
    /**
     * 5. Um algoritmo deve buscar um sub-texto dentro de um texto fornecido. 
     * (Não usar funções de busca em string).
     */

    public function execute(Request $request)
    {
        $this->validate($request, [
            'texto' => 'required|string|min:1',
            'subtexto' => 'required|string|min:1'
        ]);
        
        $texto = $request['texto'];
        $subTexto = $request['subtexto'];
        $tamanhoTexto = strlen($texto);
        $tamanhoSubTexto = strlen($subTexto);

        for ($i = 0; $i < $tamanhoTexto; $i++) {
            for ($j = 0; $j < $tamanhoSubTexto; $j++) {
                if ((string) $subTexto[$j] != (string) $texto[$i]) {
                    break;
                }
                $i += 1;
                
                if ($j == ($tamanhoSubTexto - 1)) {
                    return response()->json([
                        'encontrou' => true
                    ]);
                }
            }
        }
        return response()->json([
            'encontrou' => false
        ]);
    }
}
