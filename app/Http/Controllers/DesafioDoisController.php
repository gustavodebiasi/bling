<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesafioDoisController extends Controller
{
    public function execute(Request $request)
    {
        $this->validate($request, [
            'array' => 'required|array'
        ]);
        $array = $request['array'];

        $tamanhoArray = count($array);
        $arrayPar = [];
        $arrayImpar = [];
        $countPar = 0;
        $countImpar = 0;
        for ($i = 0; $i < $tamanhoArray; $i++) {
            if (($array[$i] % 2) == 0) {
                $arrayPar[$countPar] = $array[$i];
                $countPar++;
            } else {
                $arrayImpar[$countImpar] = $array[$i];
                $countImpar++;
            }
        }

        $arrayOrdenado = $this->ordenarArray($arrayPar);
        $arrayImpar = $this->ordenarArray($arrayImpar);

        $j = 0;
        for ($i = count($arrayOrdenado); $i < $tamanhoArray; $i++) {
            $arrayOrdenado[$i] = $arrayImpar[$j];
            $j++;
        }

        return response()->json($arrayOrdenado);
    }

    protected function ordenarArray($array)
    {
        $arrayTeste = $array;
        $tamanhoArray = count($array);
        for ($i = 0; $i < $tamanhoArray; $i++) {
            for ($j = 0; $j < $tamanhoArray-1; $j++) {
                if ($array[$j] > $array[$j+1]) {
                    $aux = $array[$j];
                    $array[$j] = $array[$j+1];
                    $array[$j+1] = $aux;
                }
            }
        }

        return $array;
    }
}
