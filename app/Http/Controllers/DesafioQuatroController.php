<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesafioQuatroController extends Controller
{
    /**
     * 4. Receba 6 números representando medidas a,b,c,d,e e f 
     * e relacionar quantos e quais triângulos é possível formar usando 
     * estas medidas. Exemplo [abc], [abd]
     */

    public function execute(Request $request)
    {
        $this->validate($request, [
            'medidas' => 'required|array|min:6|max:6'
        ]);
        $medidas = $request['medidas'];
        
		$combinacoes = $this->gerarCombinacoes($medidas);
		$triangulos = [];

		foreach ($combinacoes as $key => $combinacao) {
			if (! $this->ehTriangulo(
				$medidas[$combinacao[0]],
				$medidas[$combinacao[1]],
				$medidas[$combinacao[2]])
			) {
				continue;
			}

			$triangulos[] = [
                'combinacao' => implode(',', $combinacao),
				'medida_a' => $medidas[$combinacao[0]],
				'medida_b' => $medidas[$combinacao[1]],
				'medida_c' => $medidas[$combinacao[2]],
				'tipo_triangulo' => $this->buscarTipoTriangulo($medidas[$combinacao[0]], $medidas[$combinacao[1]], $medidas[$combinacao[2]])
			];
		}
        
        return response()->json([
            'quantidade_triangulos' => count($triangulos),
            'triangulos' => $triangulos
        ]);
	}

	protected function gerarCombinacoes($medidas)
	{
		$combinacoes = [];

		foreach ($medidas as $key1 => $medida1) {
			foreach ($medidas as $key2 => $medida2) {
				foreach ($medidas as $key3 => $medida3) {
					if ($key1 == $key2 || $key2 == $key3 || $key1 == $key3) {
						continue;
					}
					$keys = $this->ordenarKeys($key1, $key2, $key3);
					if (! isset($combinacoes[$keys])) {
						$combinacoes[$keys] = [
							$key1, $key2, $key3
						];
					}
				}
			}
		}

		return $combinacoes;
	}

	protected function ordenarKeys($key1, $key2, $key3) {
		if ($key1 > $key2) {
			$aux = $key1;
			$key1 = $key2;
			$key2 = $aux;
		}
		if ($key2 > $key3) {
			$aux = $key2;
			$key2 = $key3;
			$key3 = $aux;
		}
		if ($key1 > $key2) {
			$aux = $key1;
			$key1 = $key2;
			$key2 = $aux;
		}
		return $key1 . $key2 . $key3;
	}

	protected function ehTriangulo($a, $b, $c)
	{
		if (($a >= $b + $c) || ($b >= $a + $c) || $c >= $a + $b || $a <= 0 || $b <= 0 || $c <= 0) {
			return false;
		}
		return true;
	}

	protected function buscarTipoTriangulo($a, $b, $c)
	{
		if ($a == $b && $b == $c) {
			return 'equilátero';
		}
		if ($a == $b || $b == $c || $a == $c) {
			return 'isósceles';
		}

		return 'escaleno';
	}
}
