<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesafioSeisController extends Controller
{
    /**
     * 6. Criar um algoritmo que teste se dois retângulos se 
     * sobrepõem em um plano cartesiano e calcule a área do retângulo 
     * criado pela sobreposição. Deve receber como entrada dois 
     * retângulos formados por pontos, 
     * ex: (0,0),(2,2),(2,0),(0,2),(1,0),(1,2),(6,0),(6,2) e gerar uma saída 
     * informando a área calculada ou zero se não ocorrer sobreposição. 
     */ 
    public function execute(Request $request)
    {
        $this->validate($request, [
            'coordenadas' => 'required|array|min:4|max:4'
        ]);

        $primeiraCoordenada = explode(',', $request['coordenadas'][0]);
        $segundaCoordenada = explode(',', $request['coordenadas'][1]);
        $terceiraCoordenada = explode(',', $request['coordenadas'][2]);
        $quartaCoordenada = explode(',', $request['coordenadas'][3]);

        $matrix = [];
        $matrix = $this->preencheMatrix($matrix, $primeiraCoordenada, $segundaCoordenada);
        $matrix = $this->preencheMatrix($matrix, $terceiraCoordenada, $quartaCoordenada);

        $area = 0;
        foreach ($matrix as $linha) {
            foreach ($linha as $elemento) {
                if ($elemento > 1) {
                    $area++;
                }
            }
        }

        return response()->json([
            'area_total' => $area
        ]);
    }

    protected function preencheMatrix($matrix, $inicial, $final)
    {
        for ($i = $inicial[0]; $i <= $final[0]; $i++) {
            for ($j = $inicial[1]; $j <= $final[1]; $j++) {
                $matrix[$i][$j] = isset($matrix[$i][$j]) 
                    ? $matrix[$i][$j] += 1
                    : 1;
            }
        }
        return $matrix;
    }
}
