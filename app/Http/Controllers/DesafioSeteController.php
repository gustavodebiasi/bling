<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesafioSeteController extends Controller
{
    /**
     * Um algoritmo deve armazenar o mapa abaixo e listar 
     * os caminhos entre os pontos A e E.
     */

    public $caminhos = [
        'A' => [
            'B' => 7,
            'D' => 5,
        ],
        'B' => [
            'A' => 7,
            'C' => 8,
            'D' => 9,
            'E' => 7,
        ],
        'C' => [
            'B' => 0,
            'E' => 5,
        ],
        'D' => [
            'A' => 5,
            'B' => 9,
            'E' => 15,
            'F' => 6,
        ],
        'E' => [
            'B' => 7,
            'C' => 5,
            'D' => 15,
            'F' => 8,
        ],
        'F' => [
            'D' => 6,
            'E' => 8,
            'G' => 11,
        ],
        'G' => [
            'E' => 9,
            'F' => 11,
        ],
    ];

    public $caminhosEncontrados = [];
    public $quantidade = 0;

    public function execute()
    {
        $this->buscarLetraE('A', '');

        return response()->json([
            'quantidade' => count($this->caminhosEncontrados),
            'caminhos' => $this->caminhosEncontrados
        ]);
    }

    protected function buscarLetraE($letraAtual, $caminhoPercorrido) {
        if (strstr($caminhoPercorrido, $letraAtual)) {
            return false;
        }

        if (isset($this->caminhos[$letraAtual]['E'])) {
            $this->caminhosEncontrados[$this->quantidade] = $caminhoPercorrido . $letraAtual;
            $this->quantidade++;
        }

        foreach ($this->caminhos[$letraAtual] as $letra => $cada) {
            if ($letra != 'E') {
                $this->buscarLetraE($letra, $caminhoPercorrido . $letraAtual);
            }
        }
    }
}

