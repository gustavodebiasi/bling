<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesafioTresController extends Controller
{
    /**
     * 3 - Escreva um algoritmo que calcule o tempo em dias a partir 
     * de uma data inicial e final recebidos no formato dd/mm/aaaa. 
     * Não deve usar funções de data e hora.
     */

    public $segundosAno = 31536000;
    public $segundosDia = 86400;

    public function execute(Request $request)
    {
        $this->validate($request, [
            'data_inicial' => 'required|date_format:d/m/Y',
            'data_final' => 'required|date_format:d/m/Y'
        ]);

        $quantidadeDeDias = 0;

        $dataInicial = explode('/', $request['data_inicial']);
        $dataFinal = explode('/', $request['data_final']);

        return response()->json([ 
            'quantidadeDeDias' => $this->diferencaEntreDatas($dataInicial, $dataFinal) 
        ]);
    }

    protected function diferencaEntreDatas($dataInicial, $dataFinal)
    {
        $segundosDataInicial = $this->dataParaSegundos($dataInicial[2], $dataInicial[1], $dataInicial[0]);
        $segundosDataFinal = $this->dataParaSegundos($dataFinal[2], $dataFinal[1], $dataFinal[0]);
        return ($segundosDataFinal - $segundosDataInicial) / $this->segundosDia;
    }

    protected function dataParaSegundos($ano, $mes, $dia)
    {
        $total = ($ano - 1) * $this->segundosAno;

        $meses = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
        for ($m = 1; $m < $mes; $m++) {
            $total += ($meses[$m - 1] * $this->segundosDia);
        }

        return $total + (($dia - 1) * $this->segundosDia);
    }
}
