<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesafioUmController extends Controller
{
    public function execute(Request $request)
    {
        $this->validate($request, [
            'array' => 'required|array',
            'quantidade' => 'required|int|min:1'
        ]);
        $array = $request['array'];
        $quantidade = $request['quantidade'];

        if ($quantidade <= 0) {
            return $array;
        }

        $tamanhoArray = count($array);
        for ($i = 0; $i < $quantidade; $i++) {
            $aux = $array[0];
            for ($j = 0; $j < $tamanhoArray-1; $j++) {
                $array[$j] = $array[$j+1];
            }
            $array[$tamanhoArray-1] = $aux;
        }

        return response()->json($array);
    }
}
