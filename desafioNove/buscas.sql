-- Atores do filme com título “XYZ”
SELECT a.* FROM atores a
INNER JOIN filmes_atores fa ON fa.ator_id = a.id
INNER JOIN filmes f ON f.id = fa.filme_id
WHERE f.titulo = 'XYZ'

-- Filmes que o ator de nome “FULANO” participou
SELECT f.* FROM filmes f
INNER JOIN filmes_atores fa ON fa.filme_id = f.id
INNER JOIN atores a ON a.id = fa.ator_id
WHERE a.nome = 'FULANO'

-- Listar os filmes do ano 2015 ordenados pelo quantidade de atores --participantes e pelo título em ordem alfabética.
SELECT f.id, f.titulo, f.ano, count(fa.id)
FROM filmes f
LEFT JOIN filmes_atores fa ON fa.filme_id = f.id
WHERE f.ano = 2015
GROUP BY f.id
ORDER BY 4, f.titulo

-- Listar os atores que trabalharam em filmes cujo diretor foi “SPIELBERG”
SELECT DISTINCT(aa.id), aa.nome
FROM filmes_atores fa
INNER JOIN atores aa ON aa.id = fa.ator_id
WHERE fa.filme_id IN (SELECT f.id
    FROM filmes f
    INNER JOIN atores a ON a.id = f.diretor_id
    WHERE a.nome = 'SPIELBERG'
)