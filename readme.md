# Tecnologia

O projeto foi desenvolvido em forma de API, onde podem ser realizadas as requisições conforme documentação abaixo.
Obs.: Para facilitar, utilize o pacote do postman adicionado na raiz do projeto.

- Foi utilizado Lumen para a criação da API

# Instalação

- Instale as dependências
```
composer install --no-dev
```

# Utilização

**POST** /1-rotacionarArray
- Endpoint responsável pelo desafio 1

**POST** /2-reordenacaoDeVetor
- Endpoint responsável pelo desafio 2

**POST** /3-contarDias
- Endpoint responsável pelo desafio 3

**POST** /4-triangulos
- Endpoint responsável pelo desafio 4

**POST** /5-procuraSubTexto
- Endpoint responsável pelo desafio 5

**POST** /6-areaSobreposta
- Endpoint responsável pelo desafio 6

Obs.: Por serem retângulos, só são necessárias 2 coordenadas X,Y (ponto inicial e ponto final)

**POST** /7-caminhos
- Endpoint responsável pelo desafio 7

**DESAFIO 8** no-endpoint
- Desafio está na pasta /desafioOito

**DESAFIO 9** no-endpoint
- Desafio está na pasta /desafioNove

**PARA FACILITAR, UTILIZE O PACOTE DO POSTMAN PRESENTE NO REPOSITÓRIO**
