<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('/1-rotacionarArray', 'DesafioUmController@execute');
$router->post('/2-reordenacaoDeVetor', 'DesafioDoisController@execute');
$router->post('/3-contarDias', 'DesafioTresController@execute');
$router->post('/4-triangulos', 'DesafioQuatroController@execute');
$router->post('/5-procuraSubTexto', 'DesafioCincoController@execute');
$router->post('/6-areaSobreposta', 'DesafioSeisController@execute');
$router->post('/7-caminhos', 'DesafioSeteController@execute');
// $router->post('/8-procuraSubTexto', 'DesafioCincoController@execute');
// $router->post('/5-procuraSubTexto', 'DesafioCincoController@execute');
